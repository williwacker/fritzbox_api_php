<?php
if ( !isset($this->config) )
{
  die(__FILE__ . ' must not be called directly');
}

####################### central API config ########################
# notice: you only have to set values differing from the defaults #
###################################################################

# use the new .lua login method in current (end 2012) labor and newer firmwares (Fritz!OS 5.50 and up)
$this->config->setItem('use_lua_login_method', True);

# set to your Fritz!Box IP address or DNS name (defaults to fritz.box), for remote config mode, use the dyndns-name like example.dyndns.org
$this->config->setItem('fritzbox_ip', 'fritz.box');

# if needed, enable remote config here
#$this->config->setItem('enable_remote_config', false);
#$this->config->setItem('remote_config_user', 'test');
#$this->config->setItem('remote_config_password', 'test');

# set to write debug files
$this->config->setItem('enable_debugging', False);

# set to your Fritz!Box username, if login with username is enabled (will be ignored, when remote config is enabled)
$this->config->setItem('username', false);

# set to your Fritz!Box password (defaults to no password, will be ignored, when remote config is enabled)
#$this->config->setItem('password', '');

# set the path of the fritzbox password file
$this->config->setItem('password_file',      __DIR__ . '\password.txt');

# set the logging mechanism (defaults to console logging)
$this->config->setItem('logging', 'console'); // output to the console
#$this->config->setItem('logging', 'silent');  // do not output anything, be careful with this logging mode
#$this->config->setItem('logging', 'tam.log'); // the path to a writeable logfile

# the newline character for the logfile (does not need to be changed in most cases)
#$this->config->setItem('newline', (PHP_OS == 'WINNT') ? "\r\n" : "\n");
$this->config->setItem('newline', PHP_EOL);


############## caller backward search config ###############

# set the path of the call list for the foncalls module
$this->config->setItem('foncallslist_path', __DIR__ . '\foncallsdata.csv');

# set the path of the call list last timestamp for the foncalls module. This prevents the callback search for already processed calls.
$this->config->setItem('foncallstimestamp_path', __DIR__ . '\foncallstimestamp.dat');

# set the path of the phonebook list for the fonbook module
$this->config->setItem('fonbooklist_path', __DIR__ . '\fonbookdata.xml');

# set the path of the name not found list in public phonebook
$this->config->setItem('nameNotFoundList_path', __DIR__ . '\nameNotFound.list');

# set the number of the phone book (default is 0)
$this->config->setItem('fonbook','1');

# set the name of the phone book
# $this->config->setItem('fonbookname','Telefonbuch');
$this->config->setItem('fonbookname','Gesammelte_Anrufe');

# set the path to the sip options output file
$this->config->setItem('sip_path',           __DIR__ . '\sip.list');

# set the path to the dialrule output file
$this->config->setItem('dialrul_path',       __DIR__ . '\dialrul.list');