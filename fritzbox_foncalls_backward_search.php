 <?php

try {
	// init the output message
	$message = date('Y-m-d H:i') . ' ';

	//  load the fritzbox_api class
	require_once('fritzbox_api.class.php');
	require_once('fritzbox_utils.php');

	$fritz = new fritzbox_api();
	$sipoptions = fritzbox_get_sipoptions($fritz);
	$predial_code = fritzbox_get_dialrullist($fritz);
	if ( !$fritz->config->getItem('nameNotFoundList_path') ) {
		throw new Exception('Mandatory config Item nameNotFoundList_path not set.');
	}
	if ( ( file_exists($fritz->config->getItem('nameNotFoundList_path')) && !is_writable($fritz->config->getItem('nameNotFoundList_path')) ) || ( !file_exists($fritz->config->getItem('nameNotFoundList_path')) && !is_writable(dirname($fritz->config->getItem('nameNotFoundList_path'))) ) )
	{
		throw new Exception('Config item nameNotFoundList_path (' . $fritz->config->getItem('nameNotFoundList_path') . ') is not writeable.');
	}

	// load the calls list
	$csv = explode("\n",fritzbox_get_foncallslist($fritz));
	$numberArray = array();
	$nameNotFoundList = explode("\n",file_get_contents($fritz->config->getItem('nameNotFoundList_path')));
	$fields = "";
	unset($number_index);
	unset($name_index);
	foreach ($csv as $line) {
		if (preg_match('/^sep=(.+)/i', $line, $matches)) {
			$sep = $matches[1];
	  	} else {
	  		if ($fields == "") {
	  			$fields = $line;
				$data = explode($sep, $line);
				for ($i = 0; $i < sizeof($data); $i++) {
					if ($data[$i] == "Rufnummer") {
						$number_index = $i;
					}
					if ($data[$i] == "Name") {
						$name_index = $i;
					}
				}
	  		} else {
	  			$data = explode($sep, $line);
				if (isset($name_index) && isset($number_index) && sizeof($data) >= max($number_index,$name_index)) {
				    $mainNumber = "";
				    unset($number);
				    // caller number like this "0123456 (0123489)"
				    if (preg_match('/(\d+) \((\d+)\)/', $data[$name_index], $matches)) {
				    	$number = $data[$number_index];
				    	$mainNumber = $matches[2];
				    }
					if ($data[$name_index] == "") {
						$mainNumber = $number = $data[$number_index];
					}
					if (isset($number)) {
						// remove mobile predial code
						$number = str_replace($predial_code, "", $number);
						$found = FALSE;
						foreach ($nameNotFoundList as $ignoreNumber) {
							if ($number <> "" && $number == $ignoreNumber) {
								$found = TRUE;
							}
						}
						if (!$found && $number != "") {
							$numberArray[$number] = $mainNumber;
						}
					}
				}
	  		}
		}
	}
	if (count($numberArray) > 0) {
		$nameNotFound = FALSE;
		$nameFound = FALSE;
		foreach ($numberArray as $number => $searchNumber) {
			$name = backwardRead($searchNumber, $sipoptions['OKZPrefix'].$sipoptions['OKZ']);
            $message .= "backward search:".$searchNumber."=".$name . $fritz->config->getItem('newline');
			if (($name == "") && ($searchNumber != $number)) {
				$name = backwardRead($number, $sipoptions['OKZPrefix'].$sipoptions['OKZ']);
            	$message .= "backward search:".$number."=".$name . $fritz->config->getItem('newline');
			}
			if ($name == "") {
				array_push($nameNotFoundList,$number);
				unset($numberArray[$number]);
				$nameNotFound = TRUE;
			} else {
				$numberArray[$number] = $name;
				$nameFound = TRUE;
			}
		}
		if ($nameNotFound) {
			file_put_contents($fritz->config->getItem('nameNotFoundList_path'),implode("\n",$nameNotFoundList));
		}
		if ($nameFound) {
			fritzbox_add_fonbook_entry($fritz, $numberArray);
			fritzbox_put_fonbook($fritz);
		}
	}
	$fritz = null; // destroy the object to log out
}
catch (Exception $e) {
	$message .= $e->getMessage();
	echo($message);
}
// log the result
if ( isset($fritz) && is_object($fritz) && get_class($fritz) == 'fritzbox_api' )
{
	$fritz->logMessage($message);
}
else
{
	echo($message);
}

function backwardRead ($number, $area_code) {
	// manage local call numbers by preceeding it with the area code
	if (preg_match('/^[1-9][0-9]+/', $number)) {
		$number = preg_replace('/^[1-9][0-9]+/', "$area_code$number", $number);
	}
	unset($name);
	$name = dastelefonbuch($number);
	if (!isset($name)) {
		$name = dasoertliche($number);
	}
	return (isset($name)) ? $name : "";
}

function dastelefonbuch($number) {
	unset($name);
	$lurl=get_fcontent("http://www3.dastelefonbuch.de/?kw=$number&s=a20000&cmd=search&ort_ok=0&sp=3&vert_ok=0&aktion=23",'r');
	$line = serialize($lurl[0]);
	$line = str_replace(array("\r\n", "\r", "\n"), '', $line);
	preg_match("/notes.addNoteItem\(\"(.*?)\"/i", $line, $matches);
	if (isset($matches[1])) {
		$name  = str_replace("+", " ", $matches[1]);
		$name = trim(html_entity_decode($name));
		$name = str_replace("&", "&amp;", $name);
		return $name;
	}
}

function dasoertliche($number) {
	unset($name);
	$lurl=get_fcontent("http://www3.dasoertliche.de/Controller?quarterfilter=&zvo_ok=&book=22&plz=&quarter=&district=&ciid=&form_name=search_inv&buc=22&kgs=07331020&buab=&zbuab=&page=5&context=4&action=43&ph=$number&image=Finden",'r');
	$line = serialize($lurl[0]);
	$line = str_replace(array("\r\n", "\r", "\n"), '', $line);
	preg_match("/itemData = \[\[(.*?)\]\]/i", $line, $match1);
	if (isset($match1[1])) {
		preg_match_all('/[\']([^\1]*?)\,?[\']/', $match1[1], $match2);
		if (isset($match2[1])) {
			$name = $match2[1][14];
			$name = trim(htmlentities($name, ENT_COMPAT | ENT_HTML401, "ISO8859-15", false));
			$name = trim(html_entity_decode($name));
			$name = str_replace("&period;", ".", $name);
			$name = str_replace("&", "&amp;", $name);
			return $name;
		}
	}
}

?>
