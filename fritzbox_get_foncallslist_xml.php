<?php
/**
 * Fritz!Box PHP tools CLI script to download the calllist from the Box
 *
 * Must be called via a command line, shows a help message if called without any or an invalid argument
 * works only with older firmwares, since AVM changed the file format, on newer firmwares restults in an empty file
 * use new fritzbox_get_foncallslist.php instead
 *
 * Check the config file fritzbox.conf.php!
 *
 * @author   Gregor Nathanael Meyer <Gregor [at] der-meyer.de>
 * @license  http://creativecommons.org/licenses/by-sa/3.0/de/ Creative Commons cc-by-sa
 * @version  0.5 2015-03-23
 * @package  Fritz!Box PHP tools
 */

// load the fritzbox_api class
try
{
  // load the fritzbox_api class
  require_once('fritzbox_api.class.php');
  require_once('fritzbox_utils.php');
  $fritz = new fritzbox_api();

  // init the output message
  $message = date('Y-m-d H:i') . ' ';

  $output = fritzbox_get_foncallslist_xml($fritz);

  $fritz = null; // destroy the object to log out
  return $output;
}
catch (Exception $e)
{
  $message .= $e->getMessage();
}

// log the result
if ( isset($fritz) && is_object($fritz) && get_class($fritz) == 'fritzbox_api' )
{
  $fritz->logMessage($message);
}
else
{
  echo($message);
}
?>