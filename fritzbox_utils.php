<?php
/**
 * Fritz!Box PHP tools utility
 *
 *
 * Check the config file fritzbox.conf.php!
 *
 * @author   Werner K�hn
 * @license  http://creativecommons.org/licenses/by-sa/3.0/de/ Creative Commons cc-by-sa
 * @version  0.2 2015-03-19
 * @package  Fritz!Box PHP tools
 */


function fritzbox_add_fonbook_entry($fritz, $numberArray) {
	try
	{
		// init the output message
		$message = date('Y-m-d H:i') . ' ';

		if ( !$fritz->config->getItem('fonbooklist_path') )
		{
			throw new Exception('Mandatory config Item fonbooklist_path not set.');
		}
		if ( ( file_exists($fritz->config->getItem('fonbooklist_path')) && !is_writable($fritz->config->getItem('fonbooklist_path')) ) || ( !file_exists($fritz->config->getItem('fonbooklist_path')) && !is_writable(dirname($fritz->config->getItem('fonbooklist_path'))) ) )
		{
			throw new Exception('Config item fonbooklist_path (' . $fritz->config->getItem('fonbooklist_path') . ') is not writeable.');
		}

		$type = (isset($type)) ? $type : 'home';

		// read the fonbook as XML
		$xml = new SimpleXMLElement(fritzbox_get_fonbook($fritz));
		$phonebook = $xml->phonebook;

		foreach ($numberArray as $number => $name) {
			$type = (substr($number,0,2) == "01") ? "mobile" : "home";
			$contact = $xml->phonebook->addChild('contact');
			$contact->addChild('category', 0);
			$person = $contact->addChild('person');
			$person->addChild('realName', $name);
			$telephony = $contact->addChild('telephony');
			$telephony->addAttribute('nid', '1');
			$number = $telephony->addChild('number', $number);
			$number->addAttribute('type', $type);
//			$number->addAttribute('vanity', '');
//			$number->addAttribute('quickdial', '');
			$number->addAttribute('prio', '1');
			$number->addAttribute('id', '0');
			$contact->addChild('services');
			$contact->addChild('setup');
			$contact->addChild('mod_time', $_SERVER['REQUEST_TIME']);
		}

		$output = utf8_decode(formatPhonebookXml($xml->asXML()));
		$output = preg_replace('/utf-8/','iso-8859-1',$output);
		file_put_contents($fritz->config->getItem('fonbooklist_path'),$output);

		// set a log message
		$message .= count($numberArray) . " entry(s) successfully added" . $fritz->config->getItem('newline');
	}
	catch (Exception $e)
	{
		$message .= $e->getMessage();
	}

	// log the result
	if ( isset($fritz) && is_object($fritz) && get_class($fritz) == 'fritzbox_api' )
	{
		$fritz->logMessage($message);
	}
	else
	{
		echo($message);
	}
}

function fritzbox_put_fonbook($fritz) {
	try
	{
		// init the output message
		$message = date('Y-m-d H:i') . ' ';

		if ( !$fritz->config->getItem('fonbooklist_path') )
		{
		throw new Exception('Mandatory config Item fonbooklist_path not set.');
		}
		if ( ( file_exists($fritz->config->getItem('fonbooklist_path')) && !is_writable($fritz->config->getItem('fonbooklist_path')) ) || ( !file_exists($fritz->config->getItem('fonbooklist_path')) && !is_writable(dirname($fritz->config->getItem('fonbooklist_path'))) ) )
		{
		throw new Exception('Config item fonbooklist_path (' . $fritz->config->getItem('fonbooklist_path') . ') is not writeable.');
		}

		$params = array(
			'PhonebookId'			=> $fritz->config->getItem('fonbook'),
			'PhonebookImportFile'	=> file_get_contents($fritz->config->getItem('fonbooklist_path'))
		);
		$output = $fritz->doPostFile($params);

		// set a log message
		if (preg_match('/<h2>Fehler/',$output)) {
			$message .= "Phone book upload has failed" . $fritz->config->getItem('newline');
		} else {
			$message .= "Phone book successfully uploaded" . $fritz->config->getItem('newline');
		}
	}
	catch (Exception $e)
	{
		$message .= $e->getMessage();
	}

	// log the result
	if ( isset($fritz) && is_object($fritz) && get_class($fritz) == 'fritzbox_api' )
	{
		$fritz->logMessage($message);
	}
	else
	{
		echo($message);
	}
	return $output;
}

function fritzbox_get_fonbook($fritz) {
	try
	{
		// init the output message
		$message = date('Y-m-d H:i') . ' ';

		if ( !$fritz->config->getItem('fonbooklist_path') )
		{
			throw new Exception('Mandatory config Item fonbooklist_path not set.');
		}
		if ( ( file_exists($fritz->config->getItem('fonbooklist_path')) && !is_writable($fritz->config->getItem('fonbooklist_path')) ) || ( !file_exists($fritz->config->getItem('fonbooklist_path')) && !is_writable(dirname($fritz->config->getItem('fonbooklist_path'))) ) )
		{
			throw new Exception('Config item fonbooklist_path (' . $fritz->config->getItem('fonbooklist_path') . ') is not writeable.');
		}

		$ch = curl_init();
		$fritzCfg    = 'http://fritz.box/cgi-bin/firmwarecfg';
		$params = array(
			'sid'                   => $fritz->getSID(),
			'PhonebookId' 			=> $fritz->config->getItem('fonbook'),
			'PhonebookExportName' 	=> $fritz->config->getItem('fonbookname'),
			'PhonebookExport' 		=> "",
		);

		curl_setopt($ch, CURLOPT_URL, $fritzCfg);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = formatPhonebookXml(curl_exec($ch));
		curl_close($ch);

		// write out the phone book to the desired path
		file_put_contents($fritz->config->getItem('fonbooklist_path'), $output);

		// set a log message
		$message .= "Phone book successfully downloaded" . $fritz->config->getItem('newline');
	}
	catch (Exception $e)
	{
		$message .= $e->getMessage();
	}

	// log the result
	if ( isset($fritz) && is_object($fritz) && get_class($fritz) == 'fritzbox_api' )
	{
		$fritz->logMessage($message);
	}
	else
	{
		echo($message);
	}
	return $output;
}

function formatPhonebookXml($xml){
    $xml = preg_replace('~\s*(<([^>]*)>[^<]*</\2>|<[^>]*>)\s*~','$1',$xml);
    $xml = preg_replace('/(\?>)(<phonebooks>)(<phonebook\s[^>]*>)(\/*)/', "$1\n$2\n$3\n$4", $xml);
    $xml = preg_replace('/(<number)[\s\n](type\/*)/', "$1\n$2", $xml);
    $xml = preg_replace('/(\/*)(<\/phonebooks>)/', "$1\n$2\n", $xml);
    return $xml;
}

function fritzbox_get_fonbook_entrycount($fritz) {
	$xml = fritzbox_get_fonbook($fritz);
	$array = json_decode(json_encode((array) new SimpleXMLElement($xml)),TRUE);
	return count($array['phonebook']['contact']);
}

function fritzbox_get_foncallslist($fritz) {
	try
	{
		// init the output message
		$message = date('Y-m-d H:i') . ' ';

	  	if ( $fritz->config->getItem('enable_debugging') ) {
			if ( !$fritz->config->getItem('foncallslist_path') )
			{
				throw new Exception('Mandatory config Item foncallslist_path not set.');
			}
			if ( ( file_exists($fritz->config->getItem('foncallslist_path')) && !is_writable($fritz->config->getItem('foncallslist_path')) ) || ( !file_exists($fritz->config->getItem('foncallslist_path')) && !is_writable(dirname($fritz->config->getItem('foncallslist_path'))) ) )
			{
				throw new Exception('Config item foncallslist_path (' . $fritz->config->getItem('foncallslist_path') . ') is not writeable.');
			}
		}

		// get the phone calls list
		$params = array(
			//'getpage'         => '../html/de/home/foncallsdaten.xml',
			//'getpage'         => '../html/de/FRITZ!Box_Anrufliste.csv',
			'getpage'         => '/fon_num/foncalls_list.lua',
			'csv'             => '',
		);
		$output = $fritz->doGetRequest($params);
		if ( $fritz->config->getItem('enable_debugging') ) {
			// write out the call list to the desired path
			file_put_contents($fritz->config->getItem('foncallslist_path'), $output);
		}

		// set a log message
		$message .= "Call list successfully downloaded" . $fritz->config->getItem('newline');
	}
	catch (Exception $e)
	{
		$message .= $e->getMessage();
		echo($message);
	}
	// log the result
	if ( isset($fritz) && is_object($fritz) && get_class($fritz) == 'fritzbox_api' )
	{
		$fritz->logMessage($message);
	}
	else
	{
		echo($message);
	}
	return $output;
}

function fritzbox_get_foncallslist_xml($fritz) {
	try
	{
		// init the output message
		$message = date('Y-m-d H:i') . ' ';

	  	if ( $fritz->config->getItem('enable_debugging') ) {
			if ( !$fritz->config->getItem('foncallslist_path') )
			{
				throw new Exception('Mandatory config Item foncallslist_path not set.');
			}
			if ( ( file_exists($fritz->config->getItem('foncallslist_path')) && !is_writable($fritz->config->getItem('foncallslist_path')) ) || ( !file_exists($fritz->config->getItem('foncallslist_path')) && !is_writable(dirname($fritz->config->getItem('foncallslist_path'))) ) )
			{
				throw new Exception('Config item foncallslist_path (' . $fritz->config->getItem('foncallslist_path') . ') is not writeable.');
			}
		}

		// get the frontend-page to refresh the list
		$params = array(
			'getpage'             => '../html/de/menus/menu2.html',
			'var:menu'            => 'fon',
			'var:pagename'        => 'foncalls',
			'var:errorpagename'   => 'foncalls',
			'var:type'            => '0',
		);
		$fritz->doPostForm($params);

		// get the phone calls list
		$params = array(
			'getpage'         => '../html/de/home/foncallsdaten.xml',
		);
		$output = $fritz->doGetRequest($params);

		if ( $fritz->config->getItem('enable_debugging') ) {
			// write out the call list to the desired path
			file_put_contents($fritz->config->getItem('foncallslist_path'), $output);
		}

		// set a log message
		$message .= "Call list successfully downloaded" . $fritz->config->getItem('newline');
	}
	catch (Exception $e)
	{
		$message .= $e->getMessage();
		echo($message);
	}
	// log the result
	if ( isset($fritz) && is_object($fritz) && get_class($fritz) == 'fritzbox_api' )
	{
		$fritz->logMessage($message);
	}
	else
	{
		echo($message);
	}
	return $output;
}

function fritzbox_get_sipoptions($fritz) {
	try
	{
		// init the output message
		$message = date('Y-m-d H:i') . ' ';

	  	if ( $fritz->config->getItem('enable_debugging') ) {
			if ( !$fritz->config->getItem('sip_path') )
			{
				throw new Exception('Mandatory config item sip_path not set.');
			}
			if ( ( file_exists($fritz->config->getItem('sip_path')) && !is_writable($fritz->config->getItem('sip_path')) ) || ( !file_exists($fritz->config->getItem('sip_path')) && !is_writable(dirname($fritz->config->getItem('sip_path'))) ) )
			{
				throw new Exception('Config item sip_path (' . $fritz->config->getItem('sip_path') . ') is not writeable.');
			}
		}

		// output array
		$output = array();

		// get the list
		$params = array(
			'getpage'         => '/fon_num/sip_option.lua',
			'csv'             => '',
		);
		$sipoptions = $fritz->doGetRequest($params);

		if ( $fritz->config->getItem('enable_debugging') ) {
			// write out the sip options to the desired path
			file_put_contents($fritz->config->getItem('sip_path'), $sipoptions);
		}
		foreach (explode("\n",$sipoptions) as $line) {
			if (preg_match('/\["telcfg:settings\/Location\/(.+)"\] = "(.+)"/i', $line, $matches)) {
				$output[$matches[1]] = $matches[2];
			}
			if (preg_match('/\["telcfg:settings\/MSN\/(.+)"\] = "(.+)"/i', $line, $matches)) {
				$output[$matches[1]] = $matches[2];
			}
		}

		// set a log message
		$message .= "SIP options successfully downloaded" . $fritz->config->getItem('newline');
	}
	catch (Exception $e)
	{
		$message .= $e->getMessage();
		echo($message);
	}
	// log the result
	if ( isset($fritz) && is_object($fritz) && get_class($fritz) == 'fritzbox_api' )
	{
		$fritz->logMessage($message);
	}
	else
	{
		echo($message);
	}
	return $output;
}

function fritzbox_get_dialrullist($fritz) {
	try
	{
		// init the output message
	  	$message = date('Y-m-d H:i') . ' ';

	  	if ( $fritz->config->getItem('enable_debugging') ) {
		  	if ( !$fritz->config->getItem('dialrul_path') ) {
				throw new Exception('Mandatory config item dialrul_path not set.');
		  	}
		  	if ( ( file_exists($fritz->config->getItem('dialrul_path')) && !is_writable($fritz->config->getItem('dialrul_path')) ) || ( !file_exists($fritz->config->getItem('dialrul_path')) && !is_writable(dirname($fritz->config->getItem('dialrul_path'))) ) ) {
				throw new Exception('Config item dialrul_path (' . $fritz->config->getItem('dialrul_path') . ') is not writeable.');
		  	}
	  	}

		$output = "";

		// get the list
		$params = array(
			'getpage'         => '/fon_num/dialrul_list.lua',
			'csv'             => '',
		);
		$dialrul = $fritz->doGetRequest($params);

		if ( $fritz->config->getItem('enable_debugging') ) {
			// write out the sip options to the desired path
			file_put_contents($fritz->config->getItem('dialrul_path'), $dialrul);
		}
		$rullist_on = 0;
		foreach (explode("\n",$dialrul) as $line) {
			if (preg_match('/\["telcfg:settings\/Routing\/Group\/list\(Number,Route,Provider\)"\]/', $line) && $rullist_on != -1) {
				$rullist_on = 1;
			}
			if (preg_match('/CONFIG =/', $line)) {
				$rullist_on = -1;
			}
			if ($rullist_on >= 1) {
				if (preg_match('/\["Number"\] = "mobile"/i', $line)) {
					$rullist_on = 2;
				}
				if ($rullist_on == 2 && (preg_match('/\["Provider"\] = "(.+)"/i', $line, $matches))) {
					$provider = $matches[1];
					$rullist_on = -1;
					$pattern = "/\[\"telcfg:settings\/Routing\/Provider".$provider."\"\] = \"(.+)\"/i";
					if (preg_match($pattern, $dialrul, $matches)) {
						$output = $matches[1];
					}
				}
			}
		}

		// set a log message
		$message .= "Dial Rules successfully downloaded" . $fritz->config->getItem('newline');
	}
	catch (Exception $e)
	{
		$message .= $e->getMessage();
		echo($message);
	}
	// log the result
	if ( isset($fritz) && is_object($fritz) && get_class($fritz) == 'fritzbox_api' )
	{
		$fritz->logMessage($message);
	}
	else
	{
		echo($message);
	}
	return $output;
}

function fritzbox_get_wlanlist($fritz) {
	try
	{
		// init the output message
		$message = date('Y-m-d H:i') . ' ';

		// get the wlan list
		$params = array(
		//'getpage'             => '../html/de/menus/menu2.html',
		'getpage'             => '../net/network_user_devices.lua',
		'var:menu'            => 'net',
		'var:pagename'        => 'net',
		'var:tabNr'           => '0',
		);
		$wlan_output = explode("\n",$fritz->doPostForm($params));
		$netlist_on  = 0;
		$macArray    = array();
		$output = "";

		foreach ($wlan_output as $line)
		{
			if (preg_match('/landevice:settings/', $line) && $netlist_on != -1) {
				$netlist_on = 1;
			}
			if (preg_match('/user:settings/', $line)) {
				$netlist_on = -1;
			}
			if ($netlist_on == 1) {
				if (preg_match('/\["name"\] = "(.+)"/i', $line, $matches)) {
					$name   = $matches[1];
				}
				elseif (preg_match('/\["ip"\] = "(.+)"/i', $line, $matches)) {
					$ip     = $matches[1];
				}
				elseif (preg_match('/\["mac"\] = "([0-9a-fA-F:]+)/i', $line, $matches)) {
					$mac    = $matches[1];
				}
				elseif (preg_match('/\["active"\] = "([0-9])/i', $line, $matches)) {
					$active = $matches[1];
				}
				if (preg_match('/\}/', $line)) {
					$macArray[$mac] = array('name' => $name, 'ip' => $ip, 'state' => $active);
					$mac=$ip=$name=$active="";
				}
			}
		}

		// set a log message
		$message .= "Wlan list successfully written" . $fritz->config->getItem('newline');
	}
	catch (Exception $e)
	{
		$message .= $e->getMessage();
		echo($message);
	}
	// log the result
	if ( isset($fritz) && is_object($fritz) && get_class($fritz) == 'fritzbox_api' )
	{
		$fritz->logMessage($message);
	}
	else
	{
		echo($message);
	}
	return $macArray;
}

function get_fcontent( $url,  $javascript_loop = 0, $timeout = 5 ) {
    $url = str_replace( "&amp;", "&", urldecode(trim($url)) );

    $cookie = tempnam ("/tmp", "CURLCOOKIE");
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie );
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
    curl_setopt( $ch, CURLOPT_ENCODING, "" );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );    # required for https urls
    curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
    curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );
    curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
    $content = curl_exec( $ch );
    $response = curl_getinfo( $ch );
    curl_close ( $ch );

    if ($response['http_code'] == 301 || $response['http_code'] == 302) {
        ini_set("user_agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");

        if ( $headers = get_headers($response['url']) ) {
            foreach( $headers as $value ) {
                if ( substr( strtolower($value), 0, 9 ) == "location:" )
                    return get_url( trim( substr( $value, 9, strlen($value) ) ) );
            }
        }
    }

    if (( preg_match("/>[[:space:]]+window\.location\.replace\('(.*)'\)/i", $content, $value) || preg_match("/>[[:space:]]+window\.location\=\"(.*)\"/i", $content, $value) ) && $javascript_loop < 5) {
        return get_url( $value[1], $javascript_loop+1 );
    } else {
        return array( $content, $response );
    }
}

?>
